To set up:

cabal sandbox init
cabal sandbox install --only-dependencies --enable-tests
cabal build

To test:
cabal test

To run:
CHAT_SERVER_PORT=1234 cabal run

