-- | Test our chat server.
module Main (main) where

import Chat

import Test.Hspec

import Control.Concurrent
import System.Environment
import System.Random
import System.IO
import Network

import Data.List

withServer :: (Int -> IO ()) -> IO ()
withServer computation = do
  port <- randomRIO (1024, 50000)
  setEnv "CHAT_SERVER_PORT" (show port)
  tid <- forkIO chat
  computation port
  killThread tid
  threadDelay 10000

main :: IO ()
main = hspec $ do
  describe "connecting to server" $ do
    it "should tell me when someone joins" $ withServer $ \port -> do
      handleA <- connectTo "localhost" (PortNumber (toEnum port))
      handleB <- connectTo "localhost" (PortNumber (toEnum port))

      result <- hGetLine handleA
      result `shouldBe` "2 has joined"

      hClose handleA
      hClose handleB

    it "should tell me when someone leaves" $ withServer $ \port -> do
      handleA <- connectTo "localhost" (PortNumber (toEnum port))
      handleB <- connectTo "localhost" (PortNumber (toEnum port))

      hClose handleA

      result <- hGetLine handleB
      result `shouldBe` "1 has left"

      hClose handleB

  describe "sending a message" $ do
    it "should relay the message to the others on the server" $ withServer $ \port -> do
      handleA <- connectTo "localhost" (PortNumber (toEnum port))
      handleB <- connectTo "localhost" (PortNumber (toEnum port))

      hPutStrLn handleB "hello there"

      result <- hGetContents handleA
      ("2: hello there" `isInfixOf` result) `shouldBe` True

      hClose handleA
      hClose handleB

    it "should not be echoed back to you" $ withServer $ \port -> do
      handleA <- connectTo "localhost" (PortNumber (toEnum port))
      handleB <- connectTo "localhost" (PortNumber (toEnum port))

      _ <- hGetLine handleA

      hPutStrLn handleA "hello there"

      ready <- hWaitForInput handleA 200 
      ready `shouldBe` False

      hClose handleA
      hClose handleB




