-- | CS 223 Chat Server
-- | Jacob Sevart
-- | Repeats messages from clients to all other connected clients.
-- | Notifies all clients when someone connects or disconnects.
-- | Clients are identified numerically by the order in which they connect.
-- | There are two threads per client, a "listener" and a "talker."
-- | The architecture is based on a single broadcast channel.

module Chat (chat) where

import Control.Concurrent
import Control.Monad (forever)

import Network
import System.IO
import System.Environment (lookupEnv)
import System.Exit

import Data.Char (isControl)
import Text.Read (readMaybe)

type Username = String
type Body = String
data Message = Join Username | Leave Username | Message Username Body

instance Show Message where
  show (Join user) = user ++ " has joined"
  show (Leave user) = user ++ " has left"
  show (Message sender body) = sender ++ ": " ++ body

withPortNumber :: (Int -> IO ()) -> IO ()
withPortNumber rest = do
  var <- lookupEnv "CHAT_SERVER_PORT"
  case (var >>= readMaybe) of
    Nothing -> do
      hPutStrLn stderr "Please set the environment variable CHAT_SERVER_PORT to a suitable port number."
      exitWith (ExitFailure (0 - 1))
    Just p -> do
        rest p

chat :: IO ()
chat = withPortNumber $ \port -> do
  broadcastChan <- newChan
  listener <- listenOn $ PortNumber (toEnum port)
  iter listener broadcastChan 0 where
    iter :: Socket -> Chan Message -> Int -> IO ()
    iter listener broadcastChan n = do
      (handle, _, _) <- accept listener
      hSetBuffering handle LineBuffering
      
      let username = show $ n + 1 -- username is simply the nth client connected
      writeChan broadcastChan $ Join username

      -- each talker/listener pair gets a duplicate of the broadcast channel
      -- this way, all messages get to everyone
      chan <- dupChan broadcastChan
      
      talker <- forkIO $ talk handle username chan -- spin off a talker thread
      _ <- forkFinally (listen handle username chan) -- spin off a listener thread
        (\_ -> leave handle username chan talker) -- when it ends, call leave
      iter listener broadcastChan $ n + 1

-- | ships lines from the broadcast channel into a per-user socket
talk :: Handle -> Username -> Chan Message -> IO ()
talk h username chan = do
  --hPutStrLn h "Welcome to the chat server!"
  forever $ do
      message <- readChan chan
      case message of
        Message sender _ -> if username /= sender
                                  then hPutStrLn h $ show message
                                  else return () -- don't echo to self
        _ -> hPutStrLn h $ show message

-- | ships lines from a per-user socket into the broadcast channel
listen :: Handle -> Username -> Chan Message -> IO ()
listen h username chan = forever $ do
  line <- hGetLine h
  if not (all isControl line) -- suppresses nonsense messages on disconnect
    then writeChan chan (Message username line)
    else return ()

-- | cleans up after and announces a disconnected user
leave :: Handle -> Username -> Chan Message -> ThreadId -> IO ()
leave h username chan talker = do
  killThread talker
  writeChan chan $ Leave username
  hClose h
        
      
